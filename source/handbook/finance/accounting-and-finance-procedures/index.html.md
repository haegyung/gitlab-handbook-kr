---
layout: markdown_page
title: "Accounting and Finance Procedures"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Accounting and Finance Procedures

The pages below contain step-by-step instructions on how to execute various accounting and Finance team responsibilities. The link to our month-end close checklist can also be found below.

- [Accounts Payable, Employee Expenses, and Other Liabilities](/handbook/finance/accounts-payable-employee-expenses-and-other-liabilities/)
- [Accounts Receivable and Cash](/handbook/finance/accounts-receivable-and-cash/)
- [Financial Planning Process](/handbook/finance/financial-planning-process/)
- [Processing Payroll](/handbook/finance/processing-payroll/)
- [Intercompany Calculation](/handbook/finance/intercompany-calculation/)
- [Intercompany Settlement](/handbook/finance/intercompany-settlement/)
- [GitLab.com Cost Allocation](/handbook/finance/gitlab-com-cost-allocation/)
- [Swag Shop Transactions](/handbook/finance/swag-shop-transactions/)
- [Asset Tracking](/handbook/finance/asset-tracking/)
- [GitLab Summit Cost Tracking](/handbook/finance/gitlab-summit-cost-tracking/)
- [Month-End Close Checklist](https://docs.google.com/a/gitlab.com/spreadsheets/d/1SSUQpudxxpPgXIS97Ctuj-JRII0qhq0I3r19jmBKU7c/edit?usp=sharing)
