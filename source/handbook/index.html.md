---
layout: markdown_page
title: Team Handbook
twitter_image: '/images/tweets.-gitlab.png'
---

깃랩의 팀 핸드북은 우리가 회사를 운영하는 방법에 대한 중장 저장소입니다. 이 문서의 프린트 버젼은 [1,000 페이지가 넘는 텍스트](./tools-and-tips/#count-handbook-pages)입니다. As part of our value of being transparent the handbook is <a href="https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/handbook">open to the world</a>, and we welcome feedback<a name="feedback"></a>. Please make a <a href="https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests">merge request</a> to suggest improvements or add clarifications.
Please use <a href="https://gitlab.com/gitlab-com/www-gitlab-com/issues">issues</a> to ask questions.

* [General](.)
  * [Values](./values)
  * [General Guidelines](./general-guidelines)
  * [Handbook Usage](./handbook-usage)
  * [Communication](./communication)
  * [Security](./security)
  * [Anti-Harassment Policy](./anti-harassment)
  * [Signing legal documents](./signing-legal-documents)
  * [Tools and tips](./tools-and-tips)
  * [Leadership](./leadership)
  * [Secret Snowflake](./secret-snowflake)
  * [Using Git to update this website](./git-page-update)
* [People Operations](./people-operations)
  * [Benefits](./benefits)
  * [Code of Conduct](./people-operations/code-of-conduct/)
  * [Spending Company Money](./spending-company-money)
  * [Travel](./travel)
  * [Paid time off](./paid-time-off)
  * [Incentives](./incentives)
  * [Onboarding](./general-onboarding)
  * [Hiring](./hiring)
  * [Offboarding](./offboarding)
  * [Visas](./people-operations/visas/)
* [Engineering](./engineering)
  * [Support](./support)
  * [Infrastructure](./infrastructure)
    * [Database Team](./infrastructure/database)
    * [Gitaly Team](./infrastructure/gitaly)
    * [Production Team](./infrastructure/production)
  * [Security Team](./engineering/security)
  * [Backend](./backend)
    * [CI/CD Team](./backend#cicd)
    * [Discussion Team](./backend#discussion)
    * [Platform Team](./backend#platform)
    * [Monitoring Team](./backend#monitoring)
  * [Frontend](./frontend)
  * [Quality](./quality)
    * [Edge](./quality/edge)
  * [UX](./ux)
  * [Build](./build)
* [Marketing](./marketing)
  * [Website](./marketing/website/)
  * [Blog](./marketing/blog)
  * [Social Media Guidelines](./marketing/social-media-guidelines)
  * [Marketing and Sales Development](./marketing/marketing-sales-development/)
    * [Sales Development](./marketing/marketing-sales-development/sdr/)
    * [Content Marketing](./marketing/marketing-sales-development/content/)
    * [Field Marketing](./marketing/marketing-sales-development/field-marketing/)
    * [Marketing Operations](./marketing/marketing-sales-development/marketing-operations/)
    * [Online Marketing](./marketing/marketing-sales-development/online-marketing/)
  * [Corporate Marketing](./marketing/corporate-marketing/)
  * [Developer Relations](./marketing/developer-relations)
  * [Product Marketing](./marketing/product-marketing/)
  * [Marketing Career Development](./marketing/career-development/)
* [Sales](./sales)
  * [Account Management](./account-management)
  * [Customer Success](./customer-success/)
  * [Reseller Channels](./resellers/)
  * Sales Operations - moved to [Business Operations](./business-ops)
  * [Demo](./sales/demo/)
* [Business Operations](./business-ops)
  * [Reporting](./business-ops/reporting)
* [Finance](./finance)
  * [Stock Options](./stock-options)
  * [Board meetings](./board-meetings)
* [Product](./product)
  * [Release posts](./marketing/blog/release-posts/)
  * [Live streaming](./product/live-streaming)
  * [Making Gifs](./product/making-gifs)
  * [Data analysis](./product/data-analysis)
  * [Technical Writing](./product/technical-writing/)
  * [Markdown Guide](./product/technical-writing/markdown-guide/)
* [Legal](./legal)

<style>
.md-page h2 i.icon-color {
  color: rgb(107,79,187)
}
.md-page h2:nth-of-type(even) i.icon-color{
  color:rgb(252,109,38);
}
.font-awesome {
  font-size: .70em;
  vertical-align: middle;
  padding-bottom: 5px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 25px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 35px;
}
ul.toc-list-icons li i,
ul.toc-list-icons li ul li i {
  padding-right: 15px;
  color: rgb(107,79,187);
}
ul.toc-list-icons li:nth-of-type(even) i {
  color:rgb(252,109,38);
}
</style>
