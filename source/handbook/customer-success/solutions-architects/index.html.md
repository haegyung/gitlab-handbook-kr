---
layout: markdown_page
title: "Solutions Architects"
---

# Solutions Architect Handbook
{:.no_toc}

Solution Architects (SA) are the trusted advisors to GitLab prospects and clients, showing how the GitLab solutions address clients business requirements.

## On this page
{:.no_toc}

- TOC
{:toc}

## Role & Responsiblities
See the [Solutions Architect job description](/roles/sales/solutions-architect/)

## Before getting SA involved

#### 1) Qualification:
* What is the qualified reason to engage with us?  Technical deep dive, whiteboarding session, etc.
* What is the size/quality/state of the opportunity?
* Have you created your plan?

#### 2) Preparation:

Help us, help them.  3 ASKs:
* **Outcome:** WIIFM/T.  What’s in it for them?  Why are they looking at a new strategy for s/w dev.  We need to be able to tailor our demos and presentations to demonstrate value and how we can address their issues.
* **Infrastructure:** What i2p tools do they currently have in place?  E.g. Jenkins for CI, Ansible/Chef for automation, Codebear for review, VS/eclipse/emacs/vim for dev, etc.
* **Challenges:** What problems/roadblocks are they having?  Release velocity, visibility, collaboration, automation.

## When and How to Engage a Solutions Architect

The SA should either be included in a discovery call or provided with Outcome / Infrastructure / Challenges (see above) information uncovered by prior interactions with the account.  Occasionally, we could support a combination call of discovery and technical demonstration/deep-dive, but this is suboptimal.   However, the latter approach does not allow the SA time to prepare for and/or tailor the discussion.

### Strategic Account Engagement Protocol:

SAs are aligned regionally with Strategic Account Leaders and plan their respective account activities.  Each team collaborates according to their own Working Agreements.

### Mid-Market Engagement Protocol:

Any SA can be requested by an Account Executive using the Mid-Market process described below.  See the following *Internal Only* [Video: How to engage Solutions Architects using GitLab](https://drive.google.com/file/d/0BztS4JxtaDlrTnZIcWFhb0dWZ2c/view?ts=59879f01) for a brief overview.

1) Navigate to the SA Service Desk Board [here](https://gitlab.com/gitlab-com/customer-success/sa-service-desk/boards/339477)

> NOTE: The board is located in the Group “Customer Success” and the name of the project is “SA Service Desk”

2) Create a [new issue](https://gitlab.com/gitlab-com/customer-success/sa-service-desk/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)  
3) Use one of two templates,

*  Initial engagement, use the "SA Activity" template to ensure the proper data is collected about your needs.
*  For follow up activity, use the "Follow Up" template.

4) To help the SA group ensure success for your customer, we request the following information *prior* to SA engagement:

* **Customer Information**: Customer name, opportunity size, SFDC link
* **Preferred Date Options**: When would the customer want to have the call (including time zone information).
* **Opportunity Details**: What is the size of the opportunity?  What is the timeframe for purchase?
* **Outcome**: What's in it for them?  
* **Challenges**: What roadblocks are they facing?  
* **Infrastructure**: What tools does the customer currently use?
* **SA Tasks**: What do you need from the SA's (demo/technical shotgun/services evaluation/etc.)

5) (Optional) Associate one or more of the following labels as needed.
**Labels**
* **EXPEDITE**: This label is only to be used if there is a turnaround required prior to the next Triage call
* **Current Quarter Label**: e.g. Q2-2018
* **Services**: If this opportunity involves or requires services, use this label to indicate that.

### Weekly SA Triage Assignment
Each week, a Solutions Architect is assigned as the 'Triage SA' for the week.  This is indicated both in the `SA Triaged` label _(e.g. SA Triaged - Feb 12-16 - @dt)_ as well as in the [#solutions_architect](https://gitlab.slack.com/messages/C5D346V08/) channel in Slack.

### Triage Call
The Solutions Architect group hosts a triage call three times a week.

The current week assigned Triage SA facilitates this call.  You can find who the current triager is on the [#solutions_architect](https://gitlab.slack.com/messages/C5D346V08/) channel in Slack.

The call is on Monday, Tuesday, and Thursday at 12:30 pm Easter,n 9:30 am Pacific.  We invited the entire company to join us on these calls, and you can join with [this Zoom link](https://gitlab.zoom.us/j/918526668)

### Board Label Descriptions
* **SA Backlog**: New issues coming into the SA
* **SA Triaged**: This label will change names to show which SA is triaging this week.  Once the activity has been vetted by the SA group, approved, and SA Ownership is assigned, it is moved to this label
* **SA Doing**: Once an SA signals they are actively working the Issue, the issue is moved to SA Doing.  The issue will stay in this status until the SA and AE agree the Issue has been completed
* **SA Action Items**: These are long-running strategic issue that the SA group is owning
* **SA Waiting**: This label indicates that the SA group is waiting on the AE for more information before advancing this issue.
* **EXPEDITE**: This label is only to be used if there is a turnaround required prior to the next Triage call
* **Current Quarter Label**:
* **Services**: If this opportunity involves or requires services, use this label to indicate that.
