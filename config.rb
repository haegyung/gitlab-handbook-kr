require 'extensions/breadcrumbs'

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

set :haml, {
  ugly: true,
  format: :xhtml
}

activate :syntax, line_numbers: false

set :markdown_engine, :kramdown
set :markdown, tables: true, hard_wrap: false, input: 'GFM'

activate :autoprefixer do |config|
  config.browsers = ['last 2 versions', 'Explorer >= 9']
end

activate :breadcrumbs, wrapper: :li, separator: '', hide_home: true, convert_last: false

# Reload the browser automatically whenever files change
unless ENV['ENABLE_LIVERELOAD'] != '1'
  configure :development do
    activate :livereload
  end
end

# Build-specific configuration
configure :build do
  set :build_dir, 'public'
  activate :relative_assets
  activate :minify_css
  activate :minify_javascript
  activate :minify_html

end


page '/404.html', directory_index: false

ignore '/templates/*'
ignore '/direction/template.html'
ignore '/direction/product-vision/template.html'
ignore '/includes/*'
ignore '/releases/template.html'
ignore '/team/structure/org-chart/template.html'
ignore '/source/stylesheets/highlight.css'
ignore '/category.html'
